using Microsoft.AspNetCore.Mvc;

namespace Net6Swagger.Controllers;

/// <summary>
///     請求範例的集合區，
///     因為範例不應該時常變動，所以與主程式的邏輯切割開來。
/// </summary>
public partial class ExampleController : ControllerBase
{
    /// <summary>
    ///     Post的範例Model
    /// </summary>
    public class PostModel
    {
        /// <summary>
        ///     編號
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        ///     使用者
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        ///     回應
        /// </summary>
        public string? Message { get; set; }
    }
}