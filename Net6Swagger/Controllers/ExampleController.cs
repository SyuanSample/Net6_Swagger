using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.Filters;

namespace Net6Swagger.Controllers;

[ApiController]
[Route("[controller]")]
public partial class ExampleController : ControllerBase
{
    /// <summary>
    ///     Swagger的範例
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    [HttpPost(Name = "Post")]
    //  建立請求回應的物件格式、不包含資料。
    [SwaggerResponse(StatusCodes.Status200OK, "成功", typeof(PostModel))]
    //  建立請求、回應的範例、包含資料。
    [SwaggerRequestExample(typeof(PostModel), typeof(StringPostRequestProvider))]
    [SwaggerResponseExample(StatusCodes.Status200OK, typeof(StringPostResponseProvider))]
    public IActionResult Post(PostModel data)
    {
        data.Id += 1;
        data.Message = "Post Response Result.";
        return Ok(data);
    }
}