using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;

namespace Net6Swagger.Controllers;

/// <summary>
///     請求範例的集合區，
///     因為範例不應該時常變動，所以與主程式的邏輯切割開來。
/// </summary>
public partial class ExampleController : ControllerBase
{
    #region Post

    /// <summary>
    ///     建立回應的範例
    /// </summary>
    public class StringPostRequestProvider : IExamplesProvider<PostModel>
    {
        public PostModel GetExamples()
        {
            var model = new PostModel
            {
                Id = 1,
                Name = "Syuan"
            };
            return model;
        }
    }

    /// <summary>
    ///     建立回應的範例
    /// </summary>
    public class StringPostResponseProvider : IExamplesProvider<PostModel>
    {
        public PostModel GetExamples()
        {
            var model = new PostModel
            {
                Id = 2,
                Name = "Syuan",
                Message = "Post Response Result."
            };
            return model;
        }
    }

    #endregion
}