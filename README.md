# Net6_Swagger



## Example 範例格式
### 套件
 1. Swashbuckle.AspNetCore.Filters
 2. Swashbuckle.AspNetCore.Annotations
### ConfigureServices註冊
```c#
public static void AddSwaggerSetting(IServiceCollection services)
{
    //  建立Swagger請求回應的服務 - Add Nuget [Swashbuckle.AspNetCore.Filters]
    services.AddSwaggerExamplesFromAssemblyOf(typeof(Program));

    services.AddSwaggerGen
    (
        uiAnnotation =>
        {
            //  建立Swagger請求回應的服務 - Add Nuget [Swashbuckle.AspNetCore.Filters]
            uiAnnotation.ExampleFilters();
        }
    );
}
```
### 使用方式
#### Controller導入範例
```c#
[HttpPost(Name = "Post")]
//  建立請求回應的物件格式、不包含資料。 - Swashbuckle.AspNetCore.Annotations
[SwaggerResponse(StatusCodes.Status200OK, "成功", typeof(PostModel))]
//  建立請求、回應的範例、包含資料。 - Swashbuckle.AspNetCore.Filters
[SwaggerRequestExample(typeof(PostModel), typeof(StringPostRequestProvider))]
[SwaggerResponseExample(StatusCodes.Status200OK, typeof(StringPostResponseProvider))]
public IActionResult Post(PostModel data)
{
    data.Id += 1;
    data.Message = "Post Response Result.";
    return Ok(data);
}
```

#### 建立Example範例
```c#
 
 
 /// <summary>
 ///     建立回應的範例
 /// </summary>
 public class StringPostRequestProvider : IExamplesProvider<PostModel>
    {
        public PostModel GetExamples()
        {
            var model = new PostModel()
            {
                Id = 1,
                Name = "Syuan"
            };
            return model;
        }
    }
```

## 啟用Model的說明
### ConfigureServices註冊
```c#
public static void AddSwaggerSetting(IServiceCollection services)
{
    //  建立Swagger請求回應的服務 - Add Nuget [Swashbuckle.AspNetCore.Filters]
    services.AddSwaggerExamplesFromAssemblyOf(typeof(Program));

    services.AddSwaggerGen
    (
        uiAnnotation =>
        {
            XmlFileSetting(uiAnnotation);
        }
    );
}

```
```c#
/// <summary>
///     讀取[Assembly]自動產生Xml檔案
///     提供Swagger產生UI介面
/// </summary>
private static void XmlFileSetting(SwaggerGenOptions uiAnnotation)
{
    /*
     *  //  Controller專案的Xml File
     *  透過Assembly取得本專案的XML檔案
     *  拼裝路徑
     *  載入
     */
    var apiXmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var apiXml = Path.Combine(AppContext.BaseDirectory, apiXmlFile);
    uiAnnotation.IncludeXmlComments(apiXml);

    /*
     *  由於DomainModel會有不同專案，所以會有多個Xml File
     *  透過Assembly取得本專案的XML檔案
     *  拼裝路徑
     *  載入
     */
    // 如果要加入其他專案的xml
    //var modelXmlFile = "Model.xml";
    //var modelXml = Path.Combine(AppContext.BaseDirectory, modelXmlFile);
    //uiAnnotation.IncludeXmlComments(modelXml);
}


```

### Csproj的設定修改

#### 主專案

```xml
<!--在Csproj中加入下面三個設定。-->
<PropertyGroup>
    <!--啟用Swagger的文件格式-->
    <GenerateDocumentationFile>true</GenerateDocumentationFile>
    <!--API回應的分析-->
    <IncludeOpenAPIAnalyzers>true</IncludeOpenAPIAnalyzers>
    <!--忽略Xml註解不完全的警告-->
    <NoWarn>$(NoWarn);1591</NoWarn>
</PropertyGroup>

<!--設定Debug模式下要產生Xml檔案到指定的目錄-->
<PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Debug|AnyCPU'">
<DocumentationFile>..\ProjectNmae\XmlFile\Api.xml </DocumentationFile>
</PropertyGroup>
<!--設定Release模式下要產生Xml檔案到指定的目錄-->
<PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Release|AnyCPU'">
<DocumentationFile>..\ProjectNmae\XmlFile\\Api.xml</DocumentationFile>
</PropertyGroup>

<!--強制xml檔案每次編譯都要產生-->
<ItemGroup>
<None Update="XmlFile\Api.xml">
    <CopyToOutputDirectory>Always</CopyToOutputDirectory>
</None>
<None Update="XmlFile\SubProject.xml">
    <CopyToOutputDirectory>Always</CopyToOutputDirectory>
</None>
</ItemGroup>
```

#### 副專案
```xml

<!--設定Debug模式下要產生Xml檔案到指定的目錄-->
<PropertyGroup Condition=" '$(Configuration)' == 'Debug' ">
<DocumentationFile>..\SubProject\XmlFile\SubProject.xml</DocumentationFile>
</PropertyGroup>

      <!--設定Release模式下要產生Xml檔案到指定的目錄-->
<PropertyGroup Condition=" '$(Configuration)' == 'Release' ">
<DocumentationFile>..\SubProject\XmlFile\SubProject.xml</DocumentationFile>
</PropertyGroup>

```